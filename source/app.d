import std.stdio;
import std.file;
import std.digest.digest;
import std.process;
import std.path;

string readFile(string filename){
  return (cast(ubyte[])read(filename,2)).toHexString();
}

string probeFileType(string hexstring){
  immutable string[string] file_extension_list = [
    //images
    "FFD8": "jpeg",
    "8950": "png",
    "424D": "png", 
    "GIF8": "gif",
    //other
    "4D5A": "exe",
    "7F45": "elf",
    "2550": "pdf",
    "CAFE": "class",
  ];
  return file_extension_list[hexstring];
}

void renameFile(string filename, string filetype){
  writeln("file type is ", filetype);
  rename(filename, setExtension(filename,filetype));
}

void main(string[] args){
	if(args.length>=1){
    string filename=args[1];
    writeln("processing... ", filename);
    renameFile(filename, filename.readFile().probeFileType());
  }
}
