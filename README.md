Probe
==============

A simple utility to check if the given file is a JPEG or PNG.

Written in D with <3

Build with dmd and run it like so :

```bash
./purge filename.unknown
```
